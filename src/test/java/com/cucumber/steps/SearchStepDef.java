package com.cucumber.steps;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class SearchStepDef {
    WebDriver driver;

        @Given("^the user loads springer website in a browser$")
        public void the_user_loads_springer_website_in_a_browser() throws Throwable {
            System.setProperty("webdriver.gecko.driver", getGeckoDriver());
            driver = new FirefoxDriver();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.get("http://link.springer.com");
        }

        @When("^the user provides keyword (.*)$")
        public void the_user_provides_keyword_chemistry(String keyword) throws Throwable{
            driver.findElement(By.id("query")).clear();
            driver.findElement(By.id("query")).sendKeys(keyword);
        }

        @When("^click on search button$")
        public void click_on_search_button() throws Throwable{
            driver.findElement(By.className("search-submit")).click();
        }

        @Then("^the user should see more than (\\d+) results$")
        public void the_user_should_see_more_than_results(int count) throws Throwable{
            Assert.assertTrue(getResultCount() > count);
            driver.quit();
        }

        @Then("^the user should see (\\d+) results$")
        public void the_user_provides_keyword_mathschemistry(int count) throws Throwable{
            Assert.assertTrue(getResultCount() == count);
            driver.quit();
        }


    private int getResultCount() {
        String resultCount = driver.findElement(By.cssSelector(".number-of-search-results-and-search-terms > strong")).getText();
        resultCount = resultCount.replaceAll(",", "");
        return Integer.parseInt(resultCount);
    }

    private String getGeckoDriver(){
        if(System.getProperty("os.name").contains("Mac")){
            return "src/test/resources/geckodriver_mac";
        }else {
            return "src/test/resources/geckodriver_win.exe";
        }
    }
}
