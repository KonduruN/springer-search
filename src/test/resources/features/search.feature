Feature: Springer search

  Scenario: search with valid keyword

    Given the user loads springer website in a browser
    When the user provides keyword chemistry
    And click on search button
    Then the user should see more than 1000 results

  Scenario: search with invalid keyword

    Given the user loads springer website in a browser
    When the user provides keyword mathschemistry
    And click on search button
    Then the user should see 0 results
